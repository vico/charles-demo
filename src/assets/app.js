$('.ui.dropdown').dropdown();
$('.calendar').flatpickr({ mode: 'range'});

$('.main-nav-toggle').click(function() {
  var $nav = $('.main-nav');
  $nav.is(':visible') ? $nav.hide() : $nav.css("display", "flex");
});

$(window).resize(function() {
  var breakpoint = 767;
  var scrollbarWidth = 15;
  var display = 'none';

  if ($(window).width() + scrollbarWidth > breakpoint) {
    display = 'flex';
  } 

  $('.main-nav').css('display', display);
});

$('.current-user').click(function() {
  $('.current-user-menu').toggle();
});