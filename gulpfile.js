const gulp         = require("gulp");
const browserSync  = require("browser-sync").create();
const sass         = require("gulp-sass");
const plumber      = require("gulp-plumber");
const autoprefixer = require("gulp-autoprefixer");
const nunjucks     = require("gulp-nunjucks");
const imagemin     = require("gulp-imagemin");
const del          = require("del");

gulp.task("serve", ["copyAssets", "sass", "nj", "imagemin"], function() {
    browserSync.init({
        server: "./dist"
    });

    gulp.watch("src/scss/**/*.scss", ["sass"]);
    gulp.watch("src/**/*.html", ["nj"]);
    gulp.watch("src/images/**/*.*", ["imagemin"]);
    gulp.watch("dist/*.html").on("change", browserSync.reload);
});

gulp.task("sass", () => {
    return gulp.src("src/scss/main.scss")
        .pipe(plumber())
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ["last 3 versions"],
            cascade: false
        }))
        .pipe(gulp.dest("dist/"))
        .pipe(browserSync.stream());
});

gulp.task("nj", () => {
    return gulp.src("src/*.html")
        .pipe(plumber())
        .pipe(nunjucks.compile())
        .pipe(gulp.dest("dist"))
});

gulp.task("imagemin", () => {
    return gulp.src("src/images/**/*")
        .pipe(imagemin())
        .pipe(gulp.dest("dist/images"))
});

gulp.task("copyAssets", () => {
    return gulp.src("src/assets/**/*.*")
        .pipe(gulp.dest("dist/assets"))
});

gulp.task("clean", () => {
    return del(["dist"]);
});

gulp.task("default", ["clean"], () => {
    gulp.start("serve");
});
